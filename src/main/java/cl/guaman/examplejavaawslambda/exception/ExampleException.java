package cl.guaman.examplejavaawslambda.exception;

public class ExampleException extends RuntimeException {
    public ExampleException(String message) {
        super(message);
    }
}
