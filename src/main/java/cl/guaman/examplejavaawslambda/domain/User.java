package cl.guaman.examplejavaawslambda.domain;

import java.util.UUID;

public class User {

    private String id = UUID.randomUUID().toString();

    private String name;


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
