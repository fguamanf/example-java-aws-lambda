package cl.guaman.examplejavaawslambda.helper;

import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class LambdaHelper {

    public APIGatewayV2ProxyResponseEvent generateResponse(String body) {
        APIGatewayV2ProxyResponseEvent apiGatewayV2ProxyResponseEvent = new APIGatewayV2ProxyResponseEvent();
        apiGatewayV2ProxyResponseEvent.setBody(body);
        apiGatewayV2ProxyResponseEvent.setHeaders(getHeaders());
        apiGatewayV2ProxyResponseEvent.setStatusCode(HttpStatus.SC_OK);
        return apiGatewayV2ProxyResponseEvent;
    }

    public APIGatewayV2ProxyResponseEvent generateResponse(String body, int httpStatus) {
        APIGatewayV2ProxyResponseEvent apiGatewayV2ProxyResponseEvent = generateResponse(body);
        apiGatewayV2ProxyResponseEvent.setStatusCode(httpStatus);
        return apiGatewayV2ProxyResponseEvent;
    }

    private Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.put("X-Custom-Header", "application/json");
        return headers;
    }
}
