package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


public class SincronoDosFunction implements RequestHandler<User, User> {

    @Override
    public User handleRequest(User input, Context context) {
        return input;
    }
}

