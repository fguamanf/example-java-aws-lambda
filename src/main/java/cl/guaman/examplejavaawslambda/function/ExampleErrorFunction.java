package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.exception.ExampleException;
import cl.guaman.examplejavaawslambda.helper.LambdaHelper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;

import java.util.Objects;

public class ExampleErrorFunction implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    LambdaHelper lambdaHelper = new LambdaHelper();

    @Override
    public APIGatewayV2ProxyResponseEvent handleRequest(APIGatewayV2ProxyRequestEvent input, Context context) {
        if (Objects.isNull(input.getBody())) {
            throw new ExampleException("body is null");
        }
        return lambdaHelper.generateResponse(input.getBody());
    }
}
