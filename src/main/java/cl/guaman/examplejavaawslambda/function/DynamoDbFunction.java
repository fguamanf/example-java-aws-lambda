package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import cl.guaman.examplejavaawslambda.helper.LambdaHelper;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpStatus;

import java.util.UUID;

public class DynamoDbFunction implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    LambdaHelper lambdaHelper = new LambdaHelper();
    private DynamoDB dynamoDb;
    private String DYNAMODB_TABLE_NAME = "user";
    private Regions REGION = Regions.US_EAST_2;

    @Override
    public APIGatewayV2ProxyResponseEvent handleRequest(APIGatewayV2ProxyRequestEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        init();
        logger.log("dynamo input=" + input.getBody());
        User user = gson.fromJson(input.getBody(), User.class);
        user.setId(UUID.randomUUID().toString());
        insert(user);
        return lambdaHelper.generateResponse(gson.toJson(user), HttpStatus.SC_CREATED);
    }


    private void insert(User user) throws ConditionalCheckFailedException {
        this.dynamoDb.getTable(DYNAMODB_TABLE_NAME)
                .putItem(new PutItemSpec().withItem(new Item()
                        .withString("id", user.getId())
                        .withString("name", user.getName())));
    }

    private void init() {
        AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        client.setRegion(Region.getRegion(REGION));
        this.dynamoDb = new DynamoDB(client);
    }
}

