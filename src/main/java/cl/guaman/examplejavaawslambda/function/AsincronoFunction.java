package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import cl.guaman.examplejavaawslambda.helper.LambdaHelper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpStatus;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;

public class AsincronoFunction implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    LambdaHelper lambdaHelper = new LambdaHelper();

    @Override
    public APIGatewayV2ProxyResponseEvent handleRequest(APIGatewayV2ProxyRequestEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        String id = input.getPathParameters().get("id");
        if (Objects.isNull(id)) {
            lambdaHelper.generateResponse(input.getBody(), HttpStatus.SC_NO_CONTENT);
        }
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        User user = new User();
        Executors.newCachedThreadPool().submit(() -> {
            Thread.sleep(3000);
            completableFuture.complete("Jhon Doe");
            String name = completableFuture.get();
            user.setName(name);
            return null;
        });
        String body = gson.toJson(user);
        logger.log("asincrono body=" + body);
        return lambdaHelper.generateResponse(body);
    }
}