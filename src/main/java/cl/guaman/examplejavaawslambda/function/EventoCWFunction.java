package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class EventoCWFunction implements RequestHandler<User, String> {
    @Override
    public String handleRequest(User input, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("Hola " + input.getName());
        return "Hola " + input.getName();
    }
}