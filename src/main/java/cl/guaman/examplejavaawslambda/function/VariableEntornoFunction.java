package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import cl.guaman.examplejavaawslambda.helper.LambdaHelper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class VariableEntornoFunction implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    LambdaHelper lambdaHelper = new LambdaHelper();

    @Override
    public APIGatewayV2ProxyResponseEvent handleRequest(APIGatewayV2ProxyRequestEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        String name = System.getenv("name");
        User user = new User();
        user.setName(name);
        String body = gson.toJson(user);
        logger.log("variable entorno | body=" + body);
        return lambdaHelper.generateResponse(body);
    }
}
