package cl.guaman.examplejavaawslambda.function;

import cl.guaman.examplejavaawslambda.domain.User;
import cl.guaman.examplejavaawslambda.helper.LambdaHelper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayV2ProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpStatus;

import java.util.Objects;

public class SincronoFunction implements RequestHandler<APIGatewayV2ProxyRequestEvent, APIGatewayV2ProxyResponseEvent> {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    LambdaHelper lambdaHelper = new LambdaHelper();

    @Override
    public APIGatewayV2ProxyResponseEvent handleRequest(APIGatewayV2ProxyRequestEvent input, Context context) {
        LambdaLogger logger = context.getLogger();
        String id = input.getPathParameters().get("id");
        if (Objects.isNull(id)) {
            lambdaHelper.generateResponse(input.getBody(), HttpStatus.SC_NO_CONTENT);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
        User user = new User();
        user.setName("Jhon Doe");
        String body = gson.toJson(user);
        logger.log("sincrono body=" + body);
        return lambdaHelper.generateResponse(body);
    }
}
