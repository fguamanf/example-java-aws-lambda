[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
# AWS Lambda Java

I) configurar credenciales en consola de AWS
- https://console.aws.amazon.com/iam/home#security_credential
- ir a sección usuarios y agregar nuevo usuario
- configurar roles(lambda, s3, dynamodb, iam, y apigateway)
- descargar archivo que contiene **Access key ID** y **Secret access key**

II) instalar cliente AWS
```
curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
sudo installer -pkg AWSCLIV2.pkg -target /
```

III) ejecutar en consola
```
aws configure
```

IV) agregar
- AWS Access Key ID
- AWS Secret Access Key
- Default region name [us-east-2] 

V) Agregar en `.aws` la siguiente linea `cli_binary_format=raw-in-base64-out`
 
VI) ejecutar script
- `1-create-bucket.sh`, `2-deploy.sh`, etc